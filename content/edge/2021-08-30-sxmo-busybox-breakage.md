title: "Recent busybox upgrade breaks Sxmo"
date: 2021-08-30
---
There are reports that the recent busybox 1.34.0 upgrade breaks starting Sxmo.
Users of Sxmo are advised to hold off on upgrading for now.

** Update **
This issue was caused by a bug in busybox, which has been resolved. The fix is
in `busybox-1.34.0-r2`, found in Alpine Linux Edge.

Related:
- [12960#aports](https://gitlab.alpinelinux.org/alpine/aports/-/issues/12960)
- [busybox mailing list thread](http://lists.busybox.net/pipermail/busybox/2021-August/089128.html)
